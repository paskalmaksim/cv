FROM node:latest as builder

WORKDIR /build
COPY . /build
RUN yarn install && yarn generate

FROM nginx:latest
COPY --from=builder /build/dist /usr/share/nginx/html