const { version } = require('./package.json');

module.exports = {
  mode: 'spa',
  css: [
    "~assets/css/theme.css"
  ],
  head: {
    title: 'cv',
    htmlAttrs: {
      lang: 'en',
    },
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: 'CV' },
      { property: "og:title", content: "CV Maksim Paskal" },
      { property: "og:image", content: "/images/my.jpg" }
    ],
    link: [
      { rel: 'stylesheet', href: '//fonts.googleapis.com/css?family=Roboto' },
      { rel: 'stylesheet', href: '//use.fontawesome.com/releases/v5.0.10/css/brands.css' },
      { rel: 'stylesheet', href: '//use.fontawesome.com/releases/v5.0.10/css/solid.css' },
      { rel: 'stylesheet', href: '//use.fontawesome.com/releases/v5.0.10/css/fontawesome.css' },
      { rel: 'stylesheet', href: '//stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css' },
      { rel: 'preload', href: '/images/my.jpg', as: "image" }
    ]
  },
  modules: [
    ['@nuxtjs/google-analytics']
  ],
  'google-analytics': {
    id: 'UA-119018991-1'
  },
  loading: {
    color: '#77b6ff',
    height: '3px'
  },
  loadingIndicator: {
    name: 'circle',
    color: '#3B8070',
    background: 'white'
  },
  generate: {
    minify: {
      removeComments: true
    }
  },
  build: {
    publicPath: `/${version}/`,

    extend(config, { isDev, isClient }) {
      if (isDev && isClient) {
        config.module.rules.push({
          enforce: 'pre',
          test: /\.(js|vue)$/,
          loader: 'eslint-loader',
          exclude: /(node_modules)/
        })
      }
    }
  }
}
